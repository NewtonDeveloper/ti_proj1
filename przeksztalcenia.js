
function add_point() {
  if( typeof add_point.counter == 'undefined' ) {
        add_point.counter = 1;
    }
    ++add_point.counter;
document.getElementById('extra_points').innerHTML += 
`
Punkt:
<input type="text" size="10" value="x" id='x_cord${add_point.counter}'>
<input type="text" size="10" value="y" id='y_cord${add_point.counter}'>
<br />
`;
}


function draw_points()
{
  if( typeof add_point.counter == 'undefined' ) {
        add_point.counter = 1;
    }
 
    var i;
    for(i = 1; i<= add_point.counter; ++i)
    {
        var x = document.getElementById('x_cord'+i).value;
        var y = document.getElementById('y_cord'+i).value; 
        x = parseInt(x);
        y = parseInt(y);        
        
        x = x+ 300;
        y = -y + 250;        
 
 
        document.getElementById('draw_plain').innerHTML += `
        <circle cx="${x}" cy="${y}" r="3" stroke="blue"
         fill="blue" /> 
        `;
    }

    for(i = 1; i <= add_point.counter-1; ++i)
    {
        var x1 = document.getElementById('x_cord'+i).value;
        var y1 = document.getElementById('y_cord'+i).value; 
        var x2 = document.getElementById('x_cord'+(i+1)).value;
        var y2 = document.getElementById('y_cord'+(i+1)).value; 
        x1 = parseInt(x1);
        y1 = parseInt(y1);        
        x2 = parseInt(x2);
        y2 = parseInt(y2);        
        
        x1 = x1+ 300;
        y1 = -y1 + 250;        
        x2 = x2+ 300;
        y2 = -y2 + 250;        
 
        document.getElementById('draw_plain').innerHTML +=
        `
        <line x1="${x1}" x2="${x2}" y1="${y1}" y2="${y2}"
         stroke="blue" stroke-width="3"/>
        `;
        
        if ( i ==add_point.counter -1)
        {
            var x = document.getElementById('x_cord1').value;
            var y = document.getElementById('y_cord1').value; 
            x = parseInt(x);
            y = parseInt(y); 

            x = x+ 300;
            y = -y + 250;        
            document.getElementById('draw_plain').innerHTML +=
            `
            <line x1="${x}" x2="${x2}" y1="${y}" y2="${y2}"
             stroke="blue" stroke-width="3"/>
            `;
        }

       
    }
}

    
function show_selected()
{
    var sel = document.getElementById('convert_type');
    var choose = sel.options[sel.selectedIndex].value;

    if(choose == 'srodkowa')
    {
     document.getElementById('particular_elements').innerHTML =
    `
    Punkt środkowy:
    <input type="text" size="10" value="x" id="x_center">
    <input type="text" size="10" value="y" id="y_center">
    <br />
    `;
    }
    else if( choose == 'przesuniecie')
    {
      document.getElementById('particular_elements').innerHTML =
    `
    Wektor:
    <input type="text" size="10" value="x" id="x_center">
    <input type="text" size="10" value="y" id="y_center">
    <br />
    `;

    }
    else if( choose == 'jednokladnosc')
    {
      document.getElementById('particular_elements').innerHTML =
    `
    Punkt:
    <input type="text" size="10" value="x" id="x_center">
    <input type="text" size="10" value="y" id="y_center">
    <br />
    Skala: <input type="text" size="10" value="k" id="scale">
    <br />
    `;


    }   
    else if( choose == 'obrot')
    {
      document.getElementById('particular_elements').innerHTML =
    `
    Kąt (stopnie):
    <input type="text" size="10" value="x" id="x_center">
    <br />
    `;

    }
    show_selected.choose = choose;  
}


function transform()
{
    var choose = show_selected.choose;
    if( typeof add_point.counter == 'undefined' ) {
        add_point.counter = 1;
    }
     
     
    if(choose == 'srodkowa')
    {
        var cx = document.getElementById('x_center').value;
        var cy = document.getElementById('y_center').value; 
        cx = parseInt(cx);
        cy = parseInt(cy);
 
        var draw_cx = cx +300;
        var draw_cy = -cy + 250;
        
     
        document.getElementById('draw_plain').innerHTML +=
        `
         <circle cx="${draw_cx}" cy="${draw_cy}" r="3" stroke="black"
         fill="black" /> 
                   
        `;

        var i;

        for(i = 1; i<= add_point.counter; ++i)
        {
            var x = document.getElementById('x_cord'+i).value;
            var y = document.getElementById('y_cord'+i).value; 
            x=parseInt(x);
            y=parseInt(y);
            x= cx -(x - cx);
            y= cy -(y - cy);
            x = x+ 300;
            y = -y + 250;        
     
            document.getElementById('draw_plain').innerHTML += `
            <circle cx="${x}" cy="${y}" r="3" stroke="red"
             fill="red" /> 
            `;
        }


        for(i = 1; i <= add_point.counter-1; ++i)
        {
            var x1 = document.getElementById('x_cord'+i).value;
            var y1 = document.getElementById('y_cord'+i).value; 
            var x2 = document.getElementById('x_cord'+(i+1)).value;
            var y2 = document.getElementById('y_cord'+(i+1)).value; 
 
            x1 = parseInt(x1);
            y1 = parseInt(y1);        
            x2 = parseInt(x2);
            y2 = parseInt(y2);        
                
            x1= cx -(x1 - cx);
            y1= cy -(y1 - cy);
            x2= cx -(x2 - cx);
            y2= cy -(y2 - cy);
        
            x1 = x1+ 300;
            y1 = -y1 + 250;        
            x2 = x2+ 300;
            y2 = -y2 + 250;        
     



            document.getElementById('draw_plain').innerHTML +=
            `
            <line x1="${x1}" x2="${x2}" y1="${y1}" y2="${y2}"
             stroke="red" stroke-width="3"/>
            `;
            if ( i ==add_point.counter -1)
            {
                var x = document.getElementById('x_cord1').value;
                var y = document.getElementById('y_cord1').value; 
                x = parseInt(x);
                y = parseInt(y); 

                x= cx -(x - cx);
                y= cy -(y - cy);
 
                x = x+ 300;
                y = -y + 250;        
                document.getElementById('draw_plain').innerHTML +=
                `
                <line x1="${x}" x2="${x2}" y1="${y}" y2="${y2}"
                 stroke="red" stroke-width="3"/>
                `;
            }


        }

    }





    else if( choose == 'przesuniecie')
    {
        
        var dx = document.getElementById('x_center').value;
        var dy = document.getElementById('y_center').value; 
        dx = parseInt(dx);
        dy = parseInt(dy);        
            var i;
        for(i = 1; i<= add_point.counter; ++i)
        {
            var x = document.getElementById('x_cord'+i).value;
            var y = document.getElementById('y_cord'+i).value; 
            x=parseInt(x);
            y=parseInt(y); 
            x=x+dx;
            y=y+dy;
            x = x+ 300;
            y = -y + 250;        
     
            document.getElementById('draw_plain').innerHTML += `
            <circle cx="${x}" cy="${y}" r="3" stroke="red"
             fill="red" /> 
            `;
        }

        for(i = 1; i <= add_point.counter-1; ++i)
        {
            var x1 = document.getElementById('x_cord'+i).value;
            var y1 = document.getElementById('y_cord'+i).value; 
            var x2 = document.getElementById('x_cord'+(i+1)).value;
            var y2 = document.getElementById('y_cord'+(i+1)).value; 

            x1 = parseInt(x1);
            y1 = parseInt(y1);        
            x2 = parseInt(x2);
            y2 = parseInt(y2);        
     
            x1 = x1 +dx;
            y1 = y1 +dy;
            x2 = x2 +dx;
            y2 = y2 +dy; 
         
            x1 = x1+ 300;
            y1 = -y1 + 250;        
            x2 = x2+ 300;
            y2 = -y2 + 250;        
     
            document.getElementById('draw_plain').innerHTML +=
            `
            <line x1="${x1}" x2="${x2}" y1="${y1}" y2="${y2}"
             stroke="red" stroke-width="3"/>
            `;

        if ( i ==add_point.counter -1)
        {
            var x = document.getElementById('x_cord1').value;
            var y = document.getElementById('y_cord1').value; 
            x = parseInt(x);
            y = parseInt(y); 

            x = x +dx;
            y = y +dy;
            x = x+ 300;
            y = -y + 250;        
            document.getElementById('draw_plain').innerHTML +=
            `
            <line x1="${x}" x2="${x2}" y1="${y}" y2="${y2}"
             stroke="red" stroke-width="3"/>
            `;
        }

   

        }
        
    }

    else if( choose == 'jednokladnosc')
        {


        var cx = document.getElementById('x_center').value;
        var cy = document.getElementById('y_center').value; 
        var scale = document.getElementById('scale').value; 
        
        cx = parseInt(cx);
        cy = parseInt(cy);        
        scale = parseInt(scale) -1;

        var draw_cx = cx +300;
        var draw_cy = -cy + 250;

        document.getElementById('draw_plain').innerHTML +=
        `
         <circle cx="${draw_cx}" cy="${draw_cy}" r="3" stroke="black"
         fill="black" /> 
                   
        `;


            var i;
        for(i = 1; i<= add_point.counter; ++i)
        {
            var x = document.getElementById('x_cord'+i).value;
            var y = document.getElementById('y_cord'+i).value; 
            x=parseInt(x);
            y=parseInt(y); 
            x= (x - cx)*scale + x;
            y= (y - cy)*scale + y;;
            x = x+ 300;
            y = -y + 250;        
     
            document.getElementById('draw_plain').innerHTML += `
            <circle cx="${x}" cy="${y}" r="3" stroke="red"
             fill="red" /> 
            `;
        }

        for(i = 1; i <= add_point.counter-1; ++i)
        {
            var x1 = document.getElementById('x_cord'+i).value;
            var y1 = document.getElementById('y_cord'+i).value; 
            var x2 = document.getElementById('x_cord'+(i+1)).value;
            var y2 = document.getElementById('y_cord'+(i+1)).value; 

            x1 = parseInt(x1);
            y1 = parseInt(y1);        
            x2 = parseInt(x2);
            y2 = parseInt(y2);        

            x1= (x1 - cx)*scale + x1;
            y1= (y1 - cy)*scale + y1;
            x2= (x2 - cx)*scale + x2;
            y2= (y2 - cy)*scale + y2;
         
            x1 = x1+ 300;
            y1 = -y1 + 250;        
            x2 = x2+ 300;
            y2 = -y2 + 250;        
     
            document.getElementById('draw_plain').innerHTML +=
            `
            <line x1="${x1}" x2="${x2}" y1="${y1}" y2="${y2}"
             stroke="red" stroke-width="3"/>
            `;

        if ( i ==add_point.counter -1)
        {
            var x = document.getElementById('x_cord1').value;
            var y = document.getElementById('y_cord1').value; 
            x = parseInt(x);
            y = parseInt(y); 
            x= (x - cx)*scale + x;
            y= (y - cy)*scale + y;;
            x = x+ 300;
            y = -y + 250;        
            document.getElementById('draw_plain').innerHTML +=
            `
            <line x1="${x}" x2="${x2}" y1="${y}" y2="${y2}"
             stroke="red" stroke-width="3"/>
            `;

        }
        }   
        }   
        else if( choose == 'obrot')
        {
        var angle = document.getElementById('x_center').value;
        angle = parseInt(angle); 
        
        var i;

        for(i = 1; i<= add_point.counter; ++i)
        {
            var x = document.getElementById('x_cord'+i).value;
            var y = document.getElementById('y_cord'+i).value; 
            var alfa = angle*2*Math.PI/360.0;
            x=parseInt(x);
            y=parseInt(y);

            var x_prim=parseInt(x*Math.cos(alfa)- y*Math.sin(alfa));
            var y_prim=parseInt(x*Math.sin(alfa)+ y*Math.cos(alfa));
            x= x_prim;
            y= y_prim;
            x = x+ 300;
            y = -y + 250;        
     
            document.getElementById('draw_plain').innerHTML += `
            <circle cx="${x}" cy="${y}" r="3" stroke="red"
             fill="red" /> 
            `;
        }


        for(i = 1; i <= add_point.counter-1; ++i)
        {
            var x1 = document.getElementById('x_cord'+i).value;
            var y1 = document.getElementById('y_cord'+i).value; 
            var x2 = document.getElementById('x_cord'+(i+1)).value;
            var y2 = document.getElementById('y_cord'+(i+1)).value; 
            var alfa = angle*2*Math.PI/360.0;
 
            x1 = parseInt(x1);
            y1 = parseInt(y1);        
            x2 = parseInt(x2);
            y2 = parseInt(y2);        
                
            var x1_prim=parseInt(x1*Math.cos(alfa)
                                - y1*Math.sin(alfa));
            var y1_prim=parseInt(x1*Math.sin(alfa) 
                                + y1*Math.cos(alfa));
            var x2_prim=parseInt(x2*Math.cos(alfa)
                                - y2*Math.sin(alfa));
            var y2_prim=parseInt(x2*Math.sin(alfa) 
                                + y2*Math.cos(alfa));

            x1= x1_prim;
            y1= y1_prim;
            x2= x2_prim;
            y2= y2_prim;

            x1 = x1+ 300;
            y1 = -y1 + 250;        
            x2 = x2+ 300;
            y2 = -y2 + 250;        
     



            document.getElementById('draw_plain').innerHTML +=
            `
            <line x1="${x1}" x2="${x2}" y1="${y1}" y2="${y2}"
             stroke="red" stroke-width="3"/>
            `;
            if ( i ==add_point.counter -1)
            {
                var x = document.getElementById('x_cord1').value;
                var y = document.getElementById('y_cord1').value; 
                var alfa = angle*2*Math.PI/360.0;
              
                x = parseInt(x);
                y = parseInt(y); 
                var x_prim=parseInt(x*Math.cos(alfa)
                                - y*Math.sin(alfa));
                var y_prim=parseInt(x*Math.sin(alfa)
                                + y*Math.cos(alfa));
                x= x_prim;
                y= y_prim;
 
 
                x = x+ 300;
                y = -y + 250;        
                document.getElementById('draw_plain').innerHTML +=
                `
                <line x1="${x}" x2="${x2}" y1="${y}" y2="${y2}"
                 stroke="red" stroke-width="3"/>
                `;
            }


        }

        }

}

